#!/bin/sh
set -e
DIR="./$0"
if [ -d "$DIR" ]
then
  cp -R $DIR /etc/nixos
else
  echo "Usage: ./run.sh 1"
fi
