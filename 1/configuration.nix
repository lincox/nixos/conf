# configuration.nix
# author:  Stnby
# install: 2019-03-22

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Bootloader
  boot = {
    tmpOnTmpfs = true;
    loader = {
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };
  };

  # Networking
  networking = {
    hostName = "nixos";
    networkmanager.enable = true;
    nameservers = [ "1.1.1.1" "1.0.0.1" ];
    firewall = {
      allowedTCPPorts = [ 22 80 443 1337 ];
      #allowedUDPPorts = [ ... ];
    };
  };


  # Internationalisation
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "uk";
    defaultLocale = "en_US.UTF-8";
  };

  # Time zone.
  time.timeZone = "Europe/Vilnius";

  # Allow nonFree packages
  nixpkgs.config.allowUnfree = true;

  # Packages for system profile
  environment.systemPackages = with pkgs; [
    curl wget vim sxhkd git htop nmap feh
    firefox tdesktop rofi flameshot jq kitty
    obs-studio nodejs go mpv ffmpeg ranger
    compton redshift gimp virtualbox pavucontrol
    gnupg
  ];

  # Services
  services = {
    openssh.enable = true;
    tor.enable = true;
    xserver = {
      enable = true;
      videoDrivers = [ "nvidia" ];
      layout = "gb,lt";
      xkbOptions = "grp:ctrl_shift_toggle";
      windowManager.bspwm.enable = true;
      displayManager.auto = {
        enable = true;
        user = "stnby";
      };
    };
  };

  # ADB
  programs.adb.enable = true;

  # VirtualBox
  virtualisation.virtualbox.host.enable = true;

  # Enable sound.
  sound.enable = true;

  # Hardware
  hardware = {
    pulseaudio = {
      enable = true;
      support32Bit = true;
    };
    opengl.driSupport32Bit = true;
  };

  # Users
  users.users.stnby = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "vboxusers" "adbusers" ];
    #packages = [ ... ];
    initialPassword = "tux";
    uid = 1000;
  };

  # Mount data partition
  fileSystems."/mnt/data" = {
    device = "/dev/disk/by-label/DATAFS";
    fsType = "ext4";
  };

  system.stateVersion = "18.09";

}
